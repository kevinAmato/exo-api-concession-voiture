/*==============================================================*/
/* BDD : Concession                                             */
/*==============================================================*/
	DROP DATABASE Concession;
    CREATE DATABASE Concession;
	USE Concession;

/*==============================================================*/
/* Table : CAR                                                  */
/*==============================================================*/
create table CAR
(
   CAR_ID               int not null AUTO_INCREMENT comment'',
   MODEL                char(255) not null  comment '',
   PRIX                 decimal(10,2) not null  comment '',
   primary key (CAR_ID)
);

/*==============================================================*/
/* Table : CUSTOMER                                             */
/*==============================================================*/
create table CUSTOMER
(
   CUSTOMER_ID          int not null  AUTO_INCREMENT comment '',
   NOM                  char(255) not null  comment '',
   COORDONNEES          char(255) not null  comment '',
   primary key (CUSTOMER_ID)
);

/*==============================================================*/
/* Table : "ORDER"                                              */
/*==============================================================*/
create table ORDERS
(
   ID             int not null  AUTO_INCREMENT comment '',
   ORDER_ID             int not null  comment '',
   CUSTOMER_ID          int not null  comment '',
   CAR_ID               int  comment '',
   primary key (ID)
);

alter table ORDERS add constraint FK_ORDER_POSSEDE_CUSTOMER foreign key (CUSTOMER_ID)
      references CUSTOMER (CUSTOMER_ID) on delete restrict on update restrict;

alter table ORDERS add constraint FK_ORDER_REFERENCE_CAR foreign key (CAR_ID)
      references CAR (CAR_ID) on delete restrict on update restrict;

INSERT INTO car (MODEL, PRIX) VALUES ('Batmobil', 1750);
INSERT INTO car (MODEL, PRIX) VALUES ('Aston Martin', 990.07);
INSERT INTO car (MODEL, PRIX) VALUES ('DeLorean DMC-12', 2000);

INSERT INTO customer (NOM,COORDONNEES) VALUES ("Bruce", "Gotham");
INSERT INTO customer (NOM,COORDONNEES) VALUES ("James", "Angleterre");
INSERT INTO customer (NOM,COORDONNEES) VALUES ("Emmet", "Californie");
INSERT INTO customer (NOM,COORDONNEES) VALUES ("Kevin", "Metz");

INSERT INTO orders (ORDER_ID, CUSTOMER_ID, CAR_ID) VALUES (1, 1, 1);
INSERT INTO orders (ORDER_ID, CUSTOMER_ID, CAR_ID) VALUES (2, 2, 2);
INSERT INTO orders (ORDER_ID, CUSTOMER_ID, CAR_ID) VALUES (3, 3, 3);
INSERT INTO orders (ORDER_ID, CUSTOMER_ID, CAR_ID) VALUES (4, 4, 1);
INSERT INTO orders (ORDER_ID, CUSTOMER_ID, CAR_ID) VALUES (4, 4, 2);
INSERT INTO orders (ORDER_ID, CUSTOMER_ID, CAR_ID) VALUES (4, 4, 3);
