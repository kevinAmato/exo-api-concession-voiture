// Creation du serveur
const express = require('express')
const app = express()

// Initialisation et connexion a mysql
const mysql = require('mysql')
const db = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "0000",
    database: "concession"
})

// Connexion a la bdd
db.connect(function(err){
    console.log("Connecté à la base de données MySQL!")
})

// Middleware
app.use(express.json())

// ---------- CRUD CAR ----------

// Lecture de la table
app.get('/car', (req,res) => {

    try {
        db.query("SELECT * FROM car", function (err, result){
            res.status(200).json(result)
        })
    }
    catch {
        res.status(500)
    }

})

// Lecture d'une voiture
app.get('/car/:id', (req,res) => {

    try {
        const id = parseInt(req.params.id)

        db.query("SELECT * FROM car WHERE CAR_ID = " + id, function (err, result){
            res.status(200).json(result)
        })
    }
    catch {
        res.status(500);
    }

})

// Ajout d'une voiture
app.post('/car', (req, res) => {

    try {
        const query = "INSERT INTO car (model, prix) values (?, ?)";
        const { model, prix } = req.body

        db.query(query, [ model, prix ] , (err, result) => {
            res.status(200).json({result})
        });
    }
    catch {
        res.status(500);
    }

})


/* Tentative longue, infructueuse et non adapté

app.put('/car/:id', (req,res) => {

    const id = parseInt(req.params.id)
    const query = "UPDATE car SET ? WHERE CAR_ID = ?"
    const values = req.body
    let sqlValues = []

    for (let x in values){

        if(isNaN(values[x])){
            sqlValues.push(x + ' = ' + values[x].toString())
        } else {
            sqlValues.push(x + " = " + values[x])
        }
    }

    db.query(query, [sqlValues, id], (err, result) => {
        if(err) throw err;
        res.status(200).json({result})
    })
})
*/

// Modification d'une voiture
app.put('/car/:id', (req,res) => {

    try{
        const id = parseInt(req.params.id)
        const query = "UPDATE car SET ? WHERE ?"

        db.query(query, [req.body, {CAR_ID: id}], (err, result) => {
            res.status(200).json({result})
        })
    }
    catch {
        res.status(500);
    }

});

// Supression d'une voiture
app.delete('/car/:id', (req,res) => {

    try{
        const id = parseInt(req.params.id)

        db.query("DELETE FROM car WHERE CAR_ID = " + id, function (err, result){
            res.status(200).json({deleted:  result.affectedRows})
        })
    }
    catch {
        res.status(500);
    }

})

// ---------- CRUD CUSTOMER ----------

// lecture de la table client
app.get('/customer', (req,res) => {

    try{
        db.query("SELECT * FROM customer", function (err, result){
            res.status(200).json(result)
        })
    }
    catch {
        res.status(500);
    }

})

// Lecture d'un client
app.get('/customer/:id', (req,res) => {

    try{
        const id = parseInt(req.params.id)

        db.query("SELECT * FROM customer WHERE CUSTOMER_ID = " + id, function (err, result){
            res.status(200).json(result)
        })
    }
    catch {
        res.status(500);
    }

})

// Ajout d'un client
app.post('/customer', (req, res) => {

    try{
        const query = "INSERT INTO customer (nom, coordonnees) values (?, ?)";
        const { nom, coordonnees } = req.body

        db.query(query, [ nom, coordonnees ] , (err, result) => {
            res.status(200).json({result})
        });
    }
    catch {
        res.status(500);
    }

})

// Modification d'un client
app.put('/customer/:id', (req,res) => {

    try{
        const id = parseInt(req.params.id)
        const query = "UPDATE customer SET ? WHERE ?"

        db.query(query, [req.body, {CUSTOMER_ID: id}], (err, result) => {
            res.status(200).json({result})
        })
    }
    catch {
        res.status(500);
    }

})

// Suppression d'un client
app.delete('/customer/:id', (req,res) => {

    try{
        const id = parseInt(req.params.id)

        db.query("DELETE FROM customer WHERE CUSTOMER_ID = " + id, function (err, result){
            res.status(200).json({deleted:  result.affectedRows})
        })
    }
    catch {
        res.status(500);
    }

})

// ---------- CRUD ORDERS ----------

// Lecture de la table orders
app.get('/order', (req,res) => {

    try{
        db.query("SELECT * FROM orders", function (err, result){
            res.status(200).json(result)
        })
    }
    catch {
        res.status(500);
    }

})

// Lecture d'une commande
app.get('/order/:id', (req,res) => {

    try{
        const id = parseInt(req.params.id)

        db.query("SELECT * FROM orders WHERE ORDER_ID = " + id, function (err, result){
            res.status(200).json(result)
        })
    }
    catch {
        res.status(500);
    }

})

// Ajout d'une commande
app.post('/order', (req, res) => {

    try{
        const query = "INSERT INTO orders (order_id, customer_id, car_id) values (?, ?, ?)";
        const { order_id, customer_id, car_id } = req.body

        db.query(query, [ order_id, customer_id, car_id ] , (err, result) => {
            res.status(200).json({result})
        });
    }
    catch {
        res.status(500);
    }

})


// Modification d'une commande
app.put('/order/:id', (req,res) => {

    try{
        const id = parseInt(req.params.id)
        const query = "UPDATE orders SET ? WHERE ?"

        db.query(query, [req.body, {ID: id}], (err, result) => {
            res.status(200).json({result})
        })
    }
    catch {
        res.status(500);
    }

})

// Suppression d'une commande
app.delete('/order/:id', (req,res) => {

    try{
        const id = parseInt(req.params.id)

        db.query("DELETE FROM orders WHERE ID = " + id, function (err, result){
            res.status(200).json({deleted:  result.affectedRows})
        })
    }
    catch {
        res.status(500);
    }

})

// ---------- REQUETES SPECIFIQUES ----------

// Lecture des commandes liées à un client
app.get('/orders-customer/:id', (req,res) => {

    try{
        const id = parseInt(req.params.id)

        db.query("SELECT c.NOM, c.COORDONNEES, o.ORDER_ID FROM customer AS c, orders AS o WHERE c.CUSTOMER_ID = o.CUSTOMER_ID AND c.CUSTOMER_ID =" + id, function (err, result){
            res.status(200).json(result)
        })
    }
    catch {
        res.status(500);
    }

})

// Lecture des voitures liées à une commande
app.get('/cars-order/:id', (req,res) => {

    try{
        const id = parseInt(req.params.id)

        db.query("SELECT ORDER_ID, CUSTOMER_ID, c.CAR_ID, MODEL, PRIX FROM orders AS o, car AS c WHERE o.CAR_ID = c.CAR_ID AND ORDER_ID =" + id, function (err, result){
            res.status(200).json(result)
        })
    }
    catch {
        res.status(500);
    }

})

// ---------- FIN CRUD ----------

// Lancement du serveur
app.listen(8080, () => {
    console.log("Serveur à l'écoute")
})
